////////////////////////////////////////////////// >>Задание 4<< //////////////////////////////////////////////////

/////////////// >>прямоугольник<< ///////////////// 


document.write('<h2>прямоугольник</h2>');


let rect1 = 10;
let rect2 = 60;

for (let i = 0; i < rect1; i++) {
  for (let k = 0; k < 10; k++) {
    document.write('<div class="rectus"> </div>');
  }
  for (let j = 0; j < rect2; j++) {
    document.write('<div class="rectus">*</div>');
  }
  document.write('<br>');
}

///////////////// >>прямоугольный треугольник<< /////////////////


document.write('<br> <br> <br> <br> <br> <br>');

document.write('<h2>прямоугольный треугольник</h2>');

let tri1 = 15;
let tri2 = 20;
let counter = 1;

for (let i = 0; i < tri1; i++) {
  for (let k = 0; k < 10; k++) {
    document.write('<div class="rectus"> </div>');
  }
  for (let j = 0; j < counter; j++) {
    document.write('<div class="rectus">*</div>');
  }
  document.write('<br>');
  counter += 4;
  if (counter == tri2) break;
}


///////////////// >>равносторонний треугольник<< /////////////////


document.write('<br> <br> <br> <br> <br> <br>');

document.write('<h2>равносторонний треугольник</h2>');

let triR1 = 15;
let triR2 = 35;
counter = 1;

for (let i = 0; i < triR1; i++) {
  for (let j = 0; j < triR2; j++) {
    document.write('<div class="rectus"> </div>');
  }
  for (let k = 0; k < counter; k++) {
    document.write('<div class="rectus">*</div>');
  }
  document.write('<br>');
  triR2 -= 2;
  counter = counter + 4;
  if (triR2 == 1) break;
}


///////////////// >>ромб<< /////////////////

document.write('<br> <br> <br> <br> <br> <br>');

document.write('<h2>ромб</h2>');


let romb1 = 90;
let romb2 = 35;
counter = 1;
let reverse = false;

for (let i = 0; i < romb1; i++) {
  for (let j = 0; j < romb2; j++) {
    document.write('<div class="rectus"> </div>');
  }
  for (let k = 0; k < counter; k++) {
    document.write('<div class="rectus">*</div>');
  }
  document.write('<br>');
  (romb2 <= 1) ? (reverse = true) : '';
  if (!reverse) {
    romb2 -= 2;
    counter = counter + 4;
  } else {
    romb2 += 2;
    counter = counter - 4;
  }
  if (reverse && (romb2 > 35)) break;
}